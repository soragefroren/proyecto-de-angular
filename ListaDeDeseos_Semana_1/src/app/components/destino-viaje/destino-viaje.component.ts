import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss']
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino:DestinoViaje = new DestinoViaje('', '');
  @HostBinding('attr.class') cssClass = 'col-4 col-sm-4';
  constructor() {
  }

  ngOnInit(): void {
  }

}
