import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {
  destinos:DestinoViaje[] = [];
  lista:string[] = ['México', 'España', 'Argentina', 'Brazil'];
  constructor() {
  }
  ngOnInit(): void {
  }
  guardar(n:string,u:string):boolean {
    let d:DestinoViaje = new DestinoViaje(n, u);
    this.destinos.push(d);
    return false;
  }

}
