import { Store } from '@ngrx/store';
import { DestinoViaje } from "./destino-viaje.model";
import { FeatureDnsVs } from 'src/app/models/destinos-viajes-state.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from 'src/app/models/destinos-viajes-state.model';
import { Injectable } from '@angular/core';
// import { BehaviorSubject, Subject } from "rxjs";

@Injectable()
export class DestinosApiClient {
    private destinos:DestinoViaje[] = [];
    // private current: Subject<DestinoViaje|null> = new BehaviorSubject<DestinoViaje|null>(null);
    constructor(private store: Store<FeatureDnsVs>) { }
    add(d: DestinoViaje): void {
        /*
        let x = new DestinoViaje(d.nombre, d.imagenUrl);
        x.updateID(this.destinos.length + 1);
        this.destinos.push(x);
        */
        // this.store.dispatch(new NuevoDestinoAction(x));
        this.store.dispatch(new NuevoDestinoAction(d));
    }
    elegir(d: DestinoViaje): void {
        // d.setSelected(true);
        // this.current.next(d);
        /*
        for (let z of this.destinos) {
            if (d.id != z.id) {
                let n = new DestinoViaje(z.nombre, z.imagenUrl);
                let x = this.getById(z.id);
                n.id = x.id;
                n.setSelected(false);
                this.destinos[this.destinos.indexOf(x)] = n;
            }
        }
        let n = new DestinoViaje(d.nombre, d.imagenUrl);
        let x = this.getById(d.id);
        n.id = x.id;
        n.setSelected(true);
        this.destinos[this.destinos.indexOf(x)] = n;
        */
        // this.store.dispatch(new ElegidoFavoritoAction(n));
        this.store.dispatch(new ElegidoFavoritoAction(d));
        /*this.destinos.forEach((x:DestinoViaje) => { if (d.id != x.id) { x.setSelected(false); } });*/
    }
    /*
    getAll(): DestinoViaje[] {
        return this.destinos;
    }
    getById(id: string): DestinoViaje {
        return this.destinos.filter(function(d) {
            return d.id.toString() == id;
        })[0];
    }
    suscribeOnChange(fn: any): void{
        // this.current.subscribe(fn);
    }
    */
}
