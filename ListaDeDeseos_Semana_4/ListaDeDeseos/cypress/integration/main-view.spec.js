describe('ventana principal', () => {
    it('Tiene encabezado y el idioma es correcto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('Lista de deseos');
        cy.get('h1 b').should('contain', 'Helloes');
    });
    it('Los controles son correctos', () => {
        cy.visit('http://localhost:4200');
        cy.get('form').within(() => {
            cy.get('input[id=nombre]').type('Un texto');
            cy.get('input[id=imagenUrl]').type('Un texto');
        });
    });
    it('Hace click', () => {
        cy.visit('http://localhost:4200');
        cy.get('form').within(() => {
            cy.get('button[type="submit"]').click();
        });
    });
});